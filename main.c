#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    /*const values set, and not expected to change. MIN_VALUE and MAX_VAlUE
     are bound to the limits of the ASCII table. Lower limit 0, upper limit
     255. Basic user instructions set to a const value too. */
    
    const int MAX_VALUE = 255;
    
    const int MIN_VALUE = 0;
    
    const char INSTRUCTIONS[] = {"Please enter a number from 0 to 255: \n"};
    
    // Variable to collect the users input from scanf
    int userInput = 0;
    int conditionTest = 1;

    // Variable to interger input as character input. 
    char asciiConversion = '|';
    printf("%s", INSTRUCTIONS);
    
    scanf("%d", &userInput);

    /*Not necessary, but included. Decision test to check if input is valid.
      User input is checked against the min and max values to validate the
      input before passing it to the character variable and outputting the result.
      Failure to validate, causes an error message and program termination.
      The VALIDATION WILL ONLY trigger for SIGNED and UNSIGNED INTEGERS.
      It DOES NOT CHECK for ALPHA characters or SYMBOLS.*/ 
    if (userInput < MIN_VALUE || userInput > MAX_VALUE){

        printf("The number entered is outside of the ASCII table region.\n");
        exit(1);
        
    }else
    {
        asciiConversion = userInput;
        printf("Value %d, is equal to character", userInput);
        printf(" %c on the ASCII table.", asciiConversion);

    }
    
    return 0;
}
